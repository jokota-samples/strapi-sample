# strapi sample

strapiを使ってAPIサーバーを構築する手順を説明します。

# 1. プロジェクトを作成する

```
npx create-strapi-app@latest strapi-app
```

```
% npx create-strapi-app@latest strapi-app
? Choose your installation type Custom (manual settings)
? Please log in or sign up. Skip
? Choose your preferred language TypeScript
? Choose your default database client postgres
? Database name: strapi
? Host: 127.0.0.1
? Port: 5432
? Username: strapi
? Password: ******
? Enable SSL connection: No
```

```
Your application was created at /Users/kota/Workspace/strapi-sample/strapi-app.

Available commands in your project:

  yarn develop
  Start Strapi in watch mode. (Changes in Strapi project files will trigger a server restart)

  yarn start
  Start Strapi without watch mode.

  yarn build
  Build Strapi admin panel.

  yarn deploy
  Deploy Strapi project.

  yarn strapi
  Display all available commands.

You can start by doing:

  cd /Users/kota/Workspace/strapi-sample/strapi-app
  yarn develop
```
